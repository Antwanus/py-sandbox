matrix = [
    ['Adam', 57.6271186440678, 2.542372881355932, 0.0, 39.83050847457627],
    ['BAMON', 40.90909090909091, 9.090909090909092, 0.0, 50.0],
    ['BANESH', 72.61146496815287, 0.6369426751592356, 0.0, 26.751592356687897],
    ['BASA', 60.317460317460316, 4.761904761904762, 1.5873015873015872, 33.333333333333336],
]
col_names = ['High Income', 'Medium Income in Asia', 'Low Income', 'Extreme Income in America']

matrix.insert(0, col_names)

for e in matrix:
    if e == matrix[0]:
        print(f'{e[0]}\t\t{e[1]}\t\t{e[2]}\t\t{e[3]}')
    else:
        print(f'{e[0]}\t\t\t{round(e[1], 2)}\t\t\t\t\t\t{round(e[2], 2)}\t\t\t{round(e[3], 2)}')
