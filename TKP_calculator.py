YEARS = 10
BASE = 400_000

KBC_HCD_TKP = 1.396
KBC_PLATO_TKP = 0.69
KBC_PLATO_INIT_COST_P = 2.12
KBC_PLATO_EXIT_COST_P = 0.07

CSPX_TKP = 0.07

def calculate_costs(equity, rolling_cost_percent, timeframe_in_years, init_cost_percent=0.0, exit_cost_percent=0.0):
    """calculates the fund's total cost over amount of years"""
    result = 0
    if init_cost_percent > 0.0:
        result = equity * (init_cost_percent / 100)
    if exit_cost_percent > 0.0:
        result = result + (equity * (exit_cost_percent / 100))
    for _ in range(timeframe_in_years):
        result = result + (equity * (rolling_cost_percent / 100))

    return int(result)


kbc_horizon_comfort_dynamic = calculate_costs(BASE, KBC_HCD_TKP, YEARS)
print("kbc_horizon_comfort_dynamic:", kbc_horizon_comfort_dynamic)

kbc_plato = calculate_costs(equity=BASE, rolling_cost_percent=KBC_PLATO_TKP, timeframe_in_years=YEARS, init_cost_percent=KBC_PLATO_INIT_COST_P, exit_cost_percent=KBC_PLATO_EXIT_COST_P)
print("kbc_plato:", kbc_plato)

cpsx = calculate_costs(equity=BASE, rolling_cost_percent=CSPX_TKP, timeframe_in_years=YEARS)
print("cpsx:", cpsx)

